package Intermediate;

import java.util.Scanner;

public class Lab10 {
	 public static void main(String[] args) {

		    int n , firstTerm = 0, secondTerm = 1;
		    Scanner sc=new Scanner(System.in);
		    System.out.println("enter the nth term");
		    n=sc.nextInt();
		    System.out.println("Fibonacci Series till " + n + " terms:");

		    for (int i = 1; i <= n; ++i) {
		      System.out.print(firstTerm + ", ");

		      // compute the next term
		      int nextTerm = firstTerm + secondTerm;
		      firstTerm = secondTerm;
		      secondTerm = nextTerm;
		    }
		  }

}
