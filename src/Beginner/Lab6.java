package Beginner;

public class Lab6 {
	public static void main(String args[]) {
		int n=6;              // n is values of number     
		
		int result = 1;
		
		// Creating loop for series
		for(int i=1;i<=n;i++)
		{
			if(i%2==0)
			{
				
				System.out.print(-1*result+" ");
			}
			else
			{
			System.out.print(result+" ");
			}
			result= (int) (result+Math.pow(i, 2));
		
		}
		System.out.println();
		
		// Second Sequence
		int m=0;
		int sum=1;
		for(int i=1;i<=m;i++)
		{
			if(i%2==0) 
			{
				sum= (int) (Math.pow(i, 2));
				System.out.print(sum+" ");
			}
     	}
		System.out.println();
		
		//third sequence 
		 int n1=0,n2=1,n3,i,count=10;    //n1 is first term, n2 is second term,n3 is third term 
		 System.out.print(n2);  
		    
		 for(i=2;i<count;++i)  
		 {    
		  n3=n1+n2;    
		  System.out.print(" "+n3);    
		  n1=n2;    
		  n2=n3;    
		 }    
		
	}
	
}
