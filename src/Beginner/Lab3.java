package Beginner;

public class Lab3 {
	public static void main(String args[]) {
		int[] a = new int[] { 35, 52, 50, 60, 51, 20 }; // values of array
		int max = a[0];
		int size, temp;

		// loops for largest number
		for (int i = 1; i < a.length; i++) 
		{
			if (a[i] > max) {
				max = a[i];
			}
		}

		System.out.println("The Given Array is:");
		
		for (int i = 0; i < a.length; i++)
		{
			System.out.println(a[i]);
		}

		System.out.println("The Largest Number is:" + max);
		
		// Second largest number
		size = a.length;

		for (int i = 0; i < size; i++) 
		{
			for (int j = i + 1; j < size; j++)
			{

				if (a[i] > a[j]) {
					temp = a[i];
					a[i] = a[j];
					a[j] = temp;
				}
			}
		}
		System.out.println("Third second largest number is:: " + a[size - 2]);

	}
}
