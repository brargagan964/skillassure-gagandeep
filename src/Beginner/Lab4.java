package Beginner;

public class Lab4 {
	public static void main(String args[]) 
	{
		int n=9;                            // n is a number 
		int sum = 0;                        // sum is total of n odd numbers 
		
		for (int i = 1; i <= n; i++) 
		{
			if (i % 2 != 0) 
			{
				sum = sum + i;
			}
		}
		System.out.println("The Sum Of Odd Numbers are:" + sum);
	}

}
