package Beginner;

import java.util.Scanner;

public class Lab5 {
	public static void main(String args[]) {

		int count, sum = 0;                         //count for counting the values
		int n;                                     // n is starting range of prime number
		int m;                                      //m is end range of prime number
		 Scanner sc = new Scanner(System.in);      //Scanner used for user input values
		 System.out.println("Enter first Number");
		 n=sc.nextInt();
		 System.out.println("Enter Second Number");
		 m=sc.nextInt();
		
		for (int number = n; number <= m; number++) 
		{
			count = 0;
			
			for (int i = 2; i <= number / 2; i++) 
			{
				
				if (number % i == 0) {
					// increments the count variable by 1 if the above condition returns true
					count++;
					break;
				} 
			} 
				
			// returns true if both conditions are true
			if (count == 0 && number != 1) {
				// calculates the sum of prime numbers
				sum = sum + number;
			} 
		} 
			// prints the sum
		System.out.println("The Sum of Prime Numbers from "+ n +" to "+m+" is: " + sum);
	} 
}
